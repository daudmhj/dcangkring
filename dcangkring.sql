-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2017 at 06:38 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcangkring`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3'),
('daud', 'e5d31ab7180cd8d9e0376a9f2d67c64d');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `konten` text NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `email`, `judul`, `nama`, `konten`, `status`) VALUES
(1, 'daud.muhajir11@gmail.com', 'Tes 1', 'Daud 1', 'Konten Tes 1', 'REPLIED'),
(2, 'daud9b_09@yahoo.co.id', 'Tes 2', 'Daud 2', 'Konten Tes 2', 'UNREAD'),
(4, 'tes4@gmail.com', 'Tes 4', 'Nama 4', 'Konten Tes 4', 'UNREAD'),
(5, 'tes5@gmail.com', 'Tes 5', 'Nama 5', 'Konten Tes 5', 'UNREAD'),
(6, 'tes6@gmail.com', 'Tes 6', 'Nama 6', 'Konten Tes 6', 'UNREAD'),
(7, 'tes7@gmail.com', 'Tes 7', 'Nama 7', 'Konten Tes 7', 'UNREAD'),
(8, 'tes8@gmail.com', 'Tes 8', 'Nama 8', 'Konten Tes 8', 'UNREAD'),
(9, 'tes9@gmail.com', 'Tes 9', 'Nama 9', 'Konten Tes 9', 'UNREAD'),
(12, 'tes12@gmail.com', 'Tes 12', 'Nama 12', 'Konten Tes 12', 'READ'),
(13, 'tes13@gmail.com', 'Tes 13', 'Nama 13', 'Konten Tes 13', 'UNREAD'),
(14, 'tes14@gmail.com', 'Tes 14', 'Nama 14', 'Konten Tes 14', 'UNREAD'),
(15, 'tes15@gmail.com', 'Tes 15', 'Nama 15', 'Konten Tes 15', 'UNREAD'),
(16, 'tes16@gmail.com', 'Tes 16', 'Nama 16', 'Konten Tes 16', 'UNREAD'),
(17, 'tes17@gmail.com', 'Tes 17', 'Nama 17', 'Konten Tes 17', 'UNREAD'),
(18, 'tes18@gmail.com', 'Tes 18', 'Nama 18', 'Konten Tes 18', 'UNREAD'),
(19, 'tes19@gmail.com', 'Tes 19', 'Nama 19', 'Konten Tes 19', 'UNREAD'),
(20, 'tes20@gmail.com', 'Tes 20', 'Nama 20', 'Konten Tes 20', 'READ'),
(21, 'tes21@gmail.com', 'Tes 21', 'Nama 21', 'Konten Tes 21', 'UNREAD'),
(22, 'tes22@gmail.com', 'Tes 22', 'Nama 22', 'Konten Tes 22', 'UNREAD'),
(23, 'tes23@gmail.com', 'Tes 23', 'Nama 23', 'Konten Tes 23', 'UNREAD'),
(24, 'tes24@gmail.com', 'Tes 24', 'Nama 24', 'Konten Tes 24', 'READ'),
(25, 'tes25@gmail.com', 'Tes 25', 'Nama 25', 'Konten Tes 25', 'UNREAD'),
(26, 'tes26@gmail.com', 'Tes 26', 'Nama 26', 'Konten Tes 26', 'UNREAD'),
(27, 'tes27@gmail.com', 'Tes 27', 'Nama 27', 'Konten Tes 27', 'UNREAD'),
(28, 'tespesan@gmail.com', 'Tes', 'Testing', 'Konten Testing', 'UNREAD');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` varchar(20) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `konten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `gambar`, `konten`) VALUES
('PL01', 'Sego Kucing', 'assets/gambar/produk/str2_ccnasilemak_jg_2.jpg', 'Sego Kucing merupakan nasi mungil ukuran sekepal tangan dengan tambahan lauk sambel teri\r\n<br><br>\r\nHarga : Rp 3.000'),
('PL02', 'Sate Usus', 'assets/gambar/produk/sate-usus.jpg', 'Harga : Rp 2.000'),
('PM01', 'Sate Kerang', 'assets/gambar/produk/sate-kerang.jpg', 'Harga : Rp 3.000'),
('PM02', 'Gorengan', 'assets/gambar/produk/gorengan.png', 'Harga : Rp 1.000'),
('PM03', 'Es Teh', 'assets/gambar/produk/es-teh.jpg', 'Harga : Rp 2.000'),
('PN-02', 'Es Jeruk', 'assets/gambar/produk/es--jeruk.jpg', 'Harga : Rp 3.000'),
('PN-03', 'Coming Soon', 'assets/gambar/produk/coming_soon.png', 'Tunggu menu terbaru dari kami');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id_slider` int(11) NOT NULL,
  `header1` varchar(100) NOT NULL,
  `header2` varchar(200) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `gambar` varchar(100) NOT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `header1`, `header2`, `link`, `gambar`, `konten`) VALUES
(1, 'Apa itu D-Cangkring', 'Klik Saja Tombol Dibawah ini', '', 'assets/gambar/slider/slide1.jpg', 'D-CANGKRING\r\n\r\nMerupakan sebuah gagasan bisnis dalam bidang kuliner yang digabungkan dengan teknologi masa kini berupa website dan aplikasi mobile. \r\n\r\nDengan mengusung kuliner \"Angkringan\" yang identik dengan harganya yang terjangkau dan rasanya yang tak asing dengan lidah masyarakat Indonesia.\r\n\r\nTunggu kehadiran kami untuk menginvasi rasa penasaran dan kelaparan kalian! '),
(2, 'Aplikasi D-Cangkring', 'Beli Makanan Jadi Mudah', '', 'assets/gambar/slider/slide2.jpg', 'Aplikasi D-Cangkring akan hadir di smartphone anda, yang akan memudahkan anda dalam memesan produk kami\r\n<br>\r\nKeep in touch with us! <br>\r\nLine@ : @lmh0249z <br>\r\nFanpage : D-CANGKRING <br>\r\nInstagram : @d.cangkring <br>\r\nEmail : dcangkring17@gmail.com <br>\r\nSites : dcangkring.com'),
(3, 'Event D-Cangkring', 'Bagi-bagi Pulsa', 'cc', 'assets/gambar/slider/slide3.jpg', '[ PULSA GRATIS ] \r\n<br>\r\nHai gengs???? udah mau akhir bulan nih, kehabisan pulsa??? Pasti mau dong kalo ada yang ngasih free pulsa dengan syarat yang gampang banget!!\r\n<br>\r\nEeits tenang, this is not a clickbait! Don\'t worry about it eheheh ????\r\n<br>\r\nYuk ah langsung aja ????\r\nintip.in/dcangkringsurvey\r\n<br>\r\nBuat kalian yang beruntung akan kami umumkan lewat OA kami, just add and stay tune on :\r\n<br>\r\nHave a nice day, everyone! \r\n<br>\r\nRegards,<br> \r\nD-Cangkring');

-- --------------------------------------------------------

--
-- Table structure for table `stan`
--

CREATE TABLE `stan` (
  `id_stan` varchar(5) NOT NULL,
  `nama_stan` varchar(50) NOT NULL,
  `gmap` varchar(500) NOT NULL,
  `gambar1` varchar(200) DEFAULT NULL,
  `gambar2` varchar(200) DEFAULT NULL,
  `gambar3` varchar(200) DEFAULT NULL,
  `konten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stan`
--

INSERT INTO `stan` (`id_stan`, `nama_stan`, `gmap`, `gambar1`, `gambar2`, `gambar3`, `konten`) VALUES
('S-001', 'Angkringan Bola', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.5877934806326!2d112.79848531477508!3d-7.287650994740007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zN8KwMTcnMTUuNSJTIDExMsKwNDgnMDIuNCJF!5e0!3m2!1sen!2sus!4v1496075465686', 'assets/gambar/stan/860468.jpg', 'assets/gambar/stan/no_image.png', 'assets/gambar/stan/no_image.png', 'Bertempat di daerah Kejawan Gebang  dengan fasilitas TV yang menyiarkan siaran sepak bola'),
('S-002', 'Angkringan 116', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.595133550073!2d112.79919031477505!3d-7.286819994740591!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zN8KwMTcnMTIuNiJTIDExMsKwNDgnMDUuMCJF!5e0!3m2!1sen!2s!4v1496074996294', 'assets/gambar/stan/28-Maret,-Survey-mitra.jpg', 'assets/gambar/stan/no_image.png', 'assets/gambar/stan/no_image.png', 'Angringan yang berada di daerah kejawan gebang, tempat nya terbuka, parkiran luas. Free Wifi'),
('S-003', 'Coming Soon', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.6388872811867!2d112.79222681477492!3d-7.281864494744107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x306c3c99adedb258!2sInstitut+Teknologi+Sepuluh+Nopember!5e0!3m2!1sen!2sus!4v1496075532127', 'assets/gambar/stan/coming_soon.png', 'assets/gambar/stan/no_image.png', 'assets/gambar/stan/no_image.png', 'Tunggu Stan kami berikutnya');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `stan`
--
ALTER TABLE `stan`
  ADD PRIMARY KEY (`id_stan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
