<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
  parent::__construct();
			$this->load->model('Model_slider');
			$this->load->model('Model_produk');
			$this->load->model('Model_stan');
			$this->load->model('Model_pesan');
  }

	public function index()
	{
		$data['slider'] = $this->Model_slider->gettable_sort("slider", "id_slider");
		$data['produk'] = $this->Model_produk->gettable_sort("produk", "id_produk");
		$data['stan'] 	= $this->Model_stan->gettable_sort("stan", "id_stan");
		$this->load->view('client', $data);
	}

	public function insertPesan() {
		$this->form_validation->set_rules('email', 'EMAIL', 'required');
		$this->form_validation->set_rules('nama', 'NAMA', 'required');
		$this->form_validation->set_rules('judul', 'JUDUL', 'required');
		$this->form_validation->set_rules('konten', 'KONTEN', 'required');

		if($this->form_validation->run() === FALSE){
			//redirect('beranda');
			echo '<script language="javascript">';
			echo 'alert("Gagal mengirim pesan. Ada field yang kosong");';
			echo 'window.history.go(-1);';
			echo '</script>';
		}else{
			$nama		= $this->input->post('nama');
			$email		= $this->input->post('email');
			$judul	= $this->input->post('judul');
			$konten		= $this->input->post('konten');
			$status = "UNREAD";

			$data=array(
				'STATUS' => $status,
				'NAMA' => $nama,
				'EMAIL' => $email,
				'JUDUL' => $judul,
				'KONTEN' => $konten
				);
			$res=$this->Model_pesan->insert('pesan', $data);
			echo '<script language="javascript">';
			echo 'alert("Pesan berhasil dikirim");';
			echo 'window.history.go(-1);';
			echo '</script>';
		}
	}

	public function viewMenu($id) {
		$data['edit'] = $this->Model_produk->get_data_id($id);
		$data['produk'] = $this->Model_produk->gettable_sort("produk", "id_produk");
		$this->load->view('viewMenu', $data);
	}

	public function viewStan($id) {
		$data['edit'] = $this->Model_stan->get_data_id($id);
		$data['stan'] = $this->Model_stan->gettable_sort("stan", "id_stan");
		$this->load->view('viewStan', $data);
	}

	public function viewSlider($id){
		$data['slider'] = $this->Model_slider->get_data_id($id);
		$this->load->view('viewSlider', $data);
	}

}
