<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_admin extends CI_Model
{
    public function __construct() {
  		parent::__construct();

  	}

    function login($username, $password)
    {
        $cek = $this->db->get_where('admin', array('username'=>$username, 'password'=>md5($password)));
        if($cek->num_rows()>0)
        {
          $data['akses'] = TRUE;
          $data['username'] = $cek->row()->username;
          $data['type'] = 'admin';
        } else
        {
          $data['akses'] = FALSE;
        }
          return $data;
    }
}
